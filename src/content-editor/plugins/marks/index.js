import getRenderedMark from './marks-renderer'

export default () => {
    const renderMark = getRenderedMark();
    return ({
        renderMark
    });
}
