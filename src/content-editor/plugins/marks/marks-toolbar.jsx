import React, {Component} from 'react'
import Button from "@material-ui/core/Button";
import Icon from "@material-ui/core/Icon";

class MarksToolbarComponent extends Component {
    isMarkActive(type) {
        return this.props.editor.value.activeMarks.some(mark => mark.type === type)
    }

    renderMarkButton = (type, icon) => {
        return (
            <Button color={ this.isMarkActive(type)? "primary" : "secondary"}
                    onMouseDown={event => this.onClickMark(event, type)}
            >
                <Icon>{icon}</Icon>
            </Button>
        )
    };

    onClickMark = (event, type) => {
        event.preventDefault();
        this.props.editor.toggleMark(type);
    };

    render() {
        return (
            <div className="toolbar__marks">
                {this.renderMarkButton('bold', 'format_bold')}
                {this.renderMarkButton('italic', 'format_italic')}
                {this.renderMarkButton('underlined', 'format_underlined')}
                {this.renderMarkButton('strikethrough', 'format_strikethrough')}
            </div>
        )
    }

}

export default MarksToolbarComponent;
