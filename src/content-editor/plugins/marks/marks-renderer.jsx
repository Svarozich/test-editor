import React from "react";

export default () => (props, editor, next: () => any) => {
    const {children, mark, attributes} = props;

    switch (mark.type) {
        case 'bold':
            return <strong {...attributes}>{children}</strong>;
        case 'b':
            return <strong {...attributes}>{children}</strong>;
        case 'i':
            return <em {...attributes}>{children}</em>;
        case 'italic':
            return <em {...attributes}>{children}</em>;
        case 'underlined':
            return <u {...attributes}>{children}</u>;
        case 'u':
            return <u {...attributes}>{children}</u>;
        case 'strikethrough':
            return <s {...attributes}>{children}</s>;
        case 's':
            return <s {...attributes}>{children}</s>;
        default:
            return next();
    }
}
