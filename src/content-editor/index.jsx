import React, {Component} from 'react'
import { Editor } from 'slate-react'
import { Value } from 'slate'

import MarksToolbarComponent from './plugins/marks/marks-toolbar'
import marksPlugin from './plugins/marks/index'

const initialValue = Value.fromJSON({
    document: {
        nodes: [
            {
                object: 'block',
                type: 'paragraph',
                nodes: [
                    {
                        object: 'text',
                        text: 'A line of text in a paragraph.',
                    },
                ],
            },
        ],
    },
});

class ContentEditor extends Component {
    state = {
        value: initialValue
    };

    _plugins = [
        marksPlugin()
    ];

    ref = editor => {
        this.editor = editor
    };

    _renderEditor = (props, editor, next) => {
        const children = next();
        return (
            <React.Fragment>
                <MarksToolbarComponent editor={editor} />
                    {children}
            </React.Fragment>
        )
    };

    render() {
        return (
            <div>
                <Editor
                    plugins={this._plugins}
                    placeholder="Enter some text"
                    ref={this.ref}
                    value={this.state.value}
                    onChange={this.onChange}
                    renderEditor={this._renderEditor}
                />
            </div>
        )
    }

    onChange = ({ value }) => {
        this.setState({ value });
    };

}

export default ContentEditor;
