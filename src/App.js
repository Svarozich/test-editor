import React, {Component} from 'react';
import './App.css';
import ContentEditor from "./content-editor";

class App extends Component {
    render() {
        return (
            <div className="App">
                <header>
                    <span>Simple Text Editor</span>
                </header>
                <main>
                    <ContentEditor/>
                </main>
            </div>
        );
    }
}

export default App;
